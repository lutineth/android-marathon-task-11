package com.example.hw10

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ViewModel: ViewModel() {
    val currentText: MutableLiveData<String> by lazy{
        MutableLiveData<String>()
    }
}
