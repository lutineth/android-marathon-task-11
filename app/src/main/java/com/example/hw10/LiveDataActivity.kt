package com.example.hw10

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*

class LiveDataActivity : AppCompatActivity() {
    private lateinit var bLivedata: Button
    private lateinit var edLivedata: EditText
    private lateinit var viewModel: ViewModel
    private lateinit var tvLivedata: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_data)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        tvLivedata = findViewById(R.id.tvLivedata)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        viewModel.currentText.observe(this, Observer {
            tvLivedata.text = it.toString()
            Log.i("LiveDataActivity", "${tvLivedata.text}")
        })
        saveText()
    }

    private fun saveText(){
        bLivedata = findViewById(R.id.bLivedata)
        edLivedata = findViewById(R.id.edLivedata)

        bLivedata.setOnClickListener{
            viewModel.currentText.value = edLivedata.text.toString()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) finish()
        return true
    }
}
