package com.example.hw10

import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import android.view.*
import androidx.constraintlayout.widget.ConstraintLayout
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    private lateinit var edLastName: EditText
    private lateinit var edFirstName: EditText
    private lateinit var edMiddleName: EditText
    private lateinit var edAge: EditText
    private lateinit var edHobby: EditText
    private lateinit var button: Button
    private lateinit var background: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        edLastName = findViewById(R.id.editTextLastName)
        edFirstName = findViewById(R.id.editTextFirstName)
        edMiddleName = findViewById(R.id.editTextMiddleName)
        edAge = findViewById(R.id.editTextAge)
        edHobby = findViewById(R.id.editTextHobby)

        loadInfo()

        button = findViewById(R.id.buttonInfo)
        button.setOnClickListener{
            PersonalInfo.lastName = edLastName.text.toString()
            PersonalInfo.firstName= edFirstName.text.toString()
            PersonalInfo.middleName = edMiddleName.text.toString()
            PersonalInfo.age = edAge.text.toString()
            PersonalInfo.hobby = edHobby.text.toString()
            Intent(this, InfoActivity::class.java).also {
                startActivity(it)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_ma, menu)
        return true
    }

    override fun onOptionsItemSelected(option: MenuItem): Boolean {
        val pics = listOf(R.drawable.art_piece, R.drawable.city, R.drawable.planet, R.drawable.gothic)
        when(option.itemId){
            R.id.next -> {
                Intent(this, LiveDataActivity::class.java).also {
                    startActivity(it)
                }
            }
            R.id.random -> {
                val rand = Random.nextInt(pics.size)
                background = findViewById(R.id.igBackground)
                background.setBackgroundResource(pics[rand])
            }
            R.id.save -> writeInfo()
        }
        return true
    }

    private fun writeInfo(){
        val lastName = edLastName.text.toString()
        val firstName = edFirstName.text.toString()
        val middleName = edMiddleName.text.toString()
        val age = edAge.text.toString()
        val hobby = edHobby.text.toString()
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val write = sharedPreferences.edit()

        write.apply{
            putString(PersonalInfo.lastName, lastName)
            putString(PersonalInfo.firstName, firstName)
            putString(PersonalInfo.middleName, middleName)
            putString(PersonalInfo.age, age)
            putString(PersonalInfo.hobby, hobby)
        }.apply()
    }

    private fun loadInfo(){
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        edLastName.setText(sharedPreferences.getString(PersonalInfo.lastName, ""))
        edFirstName.setText(sharedPreferences.getString(PersonalInfo.firstName, ""))
        edMiddleName.setText(sharedPreferences.getString(PersonalInfo.middleName, ""))
        edAge.setText(sharedPreferences.getString(PersonalInfo.age, ""))
        edHobby.setText(sharedPreferences.getString(PersonalInfo.hobby, ""))
    }
}
